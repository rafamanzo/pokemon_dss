# frozen_string_literal: true

Then(/^the following species should exist:$/) do |table|
  table.hashes.each do |row|
    expect(Object.const_defined?("Species::#{row['name']}")).to eq(true)
    expect(
      Object.const_get("Species::#{row['name']}").new
    ).to be_a(Species::Base)
  end
end
