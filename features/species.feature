Feature: Pokémon Species
  In order to have information about my pokémons
  As a true Pokémon fan
  Then I should know each species

  Scenario: checking known species
    Then the following species should exist:
      | name       |
      | Charmander |
