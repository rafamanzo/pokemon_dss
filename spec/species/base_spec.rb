# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Species::Base do
  describe 'method' do
    describe 'name' do
      it 'is expected to to be the same as of the class' do
        expect(subject.name).to eq('Base')
      end
    end
  end
end
