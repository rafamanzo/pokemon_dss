# frozen_string_literal: true

require_relative 'species/base'
require_relative 'species/charmander'

# This module will hold each known species of pokemon
module Species; end
