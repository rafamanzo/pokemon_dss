# frozen_string_literal: true

module Species
  # Base class for each specie
  class Base
    def name
      self.class.name.split('::').last
    end
  end
end
